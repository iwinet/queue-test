<?php

namespace AppBundle\Command;

use Enqueue\Client\Producer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LetThemEatCakeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:let-them-eat-cake')
            ->setDescription('Bakes cookies, fruit cake and pumpkin pie to feed the masses')
            ->setHelp('This command adds a selection of baked goods to the queue')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("I shall feed you all");

        /** @var Producer $producer */
        $producer = $this->getContainer()->get('enqueue.producer');
        $item_number = 0;
        for ($i = 0; $i < 100; $i++) {

            // bake a few cookies
            for ($j = 0; $j < 3; $j++) {
                $cookie = ++$item_number;
                $producer->sendEvent('cookie_channel', [
                    'cookie' => $cookie,
                    'bites' => rand(1, 2)
                ]);
                $output->writeln('open up for cookie ' . $cookie);
            }

            // bake a fruit_cake
            $fruit_cake = ++$item_number;
            $producer->sendEvent('fruit_cake_channel', [
                'fruit_cake' => $fruit_cake,
                'bites' => rand(2,5)
            ]);
            $output->writeln('open up for fruit_cake ' . $fruit_cake);

            // bake a pumpkin_pie
            $pumpkin_pie = ++$item_number;
            $producer->sendEvent('pumpkin_pie_channel', [
                'pumpkin_pie' => $pumpkin_pie,
                'bites' => rand(3,6)
            ]);
            $output->writeln('open up for pumpkin_pie ' . $pumpkin_pie);

            // take a rest
            sleep(rand(1,3));
        }
    }
}
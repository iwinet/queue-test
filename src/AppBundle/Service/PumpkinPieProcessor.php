<?php

namespace AppBundle\Service;

use Enqueue\Client\TopicSubscriberInterface;
use Interop\Queue\PsrContext;
use Interop\Queue\PsrMessage;
use Interop\Queue\PsrProcessor;

class PumpkinPieProcessor implements PsrProcessor, TopicSubscriberInterface
{

    /**
     * The method has to return either self::ACK, self::REJECT, self::REQUEUE string.
     *
     * The method also can return an object.
     * It must implement __toString method and the method must return one of the constants from above.
     *
     * @param PsrMessage $message
     * @param PsrContext $context
     *
     * @return string|object with __toString method implemented
     */
    public function process(PsrMessage $message, PsrContext $context)
    {
        $data = json_decode($message->getBody());
        echo "Hipster alert, do you have a latte to go with that pumpkin pie {$data->pumpkin_pie} ";
        if (rand(0,6) === 6) {
            echo " I'm full, saving it for later\n";
            return self::REQUEUE; // the message is fine but you want to postpone processing
        } else {
            for ($i = 0; $i < $data->bites; $i++) {
                echo '.';
                sleep(1);
            }
            echo " nom!\n";

            return self::ACK;
        }
        // return self::REJECT; // when the message is broken
    }

    /**
     * The result maybe either:.
     *
     * ['aTopicName']
     *
     * or
     *
     * ['aTopicName' => [
     *     'processorName' => 'processor',
     *     'queueName' => 'a_client_queue_name',
     *     'queueNameHardcoded' => true,
     *   ]]
     *
     * processorName, queueName and queueNameHardcoded are optional.
     *
     * Note: If you set queueNameHardcoded to true then the queueName is used as is and therefor the driver is not used to create a transport queue name.
     *
     * @return array
     */
    public static function getSubscribedTopics()
    {
        return ['pumpkin_pie_channel'];
    }
}
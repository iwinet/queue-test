# Testing a queue

### Motivation & analogy

We want to venture in to the world of microservices or at least start using a queue to remove processes from 
paths that we would like to speed up: mainly processing transactions. A transaction **needs** to be saved 
immediately, but things like: sending mails, mutating stocks and tracking customer loyalty, is something that 
has to be done, but it does not have to be done instantly. So it would be nice if we can just put it aside 
and work on it later.
We're also interested in log consolidation and gather useful application data.

We are using:
- [Symfony](http://symfony.com/) 3.3
- [enqueue bundle](https://github.com/php-enqueue/enqueue-bundle) (more flexible and easier to setup than RabbitmqBundle) 
- [rabbitmq](http://www.rabbitmq.com/) as queueing middleware
- [docker-compose](https://docs.docker.com/compose/) to orchestrate the containers
- [fluentd](https://www.fluentd.org/) as log consolidator
- [elasticsearch](https://www.elastic.co/products/elasticsearch) and [kibana](https://www.elastic.co/products/kibana) to give insights in the data

This demo uses the analogy of someone providing different foods and having a lot of consumers eating it all up.
The foods are cookies, pumpkin pies and fruit cakes. Each has a range of bites it takes to eat them 
(each bite takes a second in the consumer).

### Usage
To use this:
- install Docker (for Mac for example) <https://www.docker.com/get-docker>
- run `./init.sh`, this takes some time (5 minutes?) as it needs to download images and build some 
  as well and run composer install. See the code for what it does exactly.
- go to <http://localhost:15672/> with guest / guest to see the queue
- do `tail -f logs/app.*.log` to see logging of all app containers
- or go to <http://localhost:5601/> to see kibana, add `app-*` as index and go browse 'Discover' 
- do `./feed.sh` to put stuff in the queue or `docker exec -it app /app/bin/console 'app:let-them-eat-cake'`
feed generates 500 event of three types at a steady tempo
- if you need more processors do `docker-compose up -d --scale app=16` for example
- go to <http://localhost:8080/> to view cAdvisor's view of the containers
- destroy everything with `./clear.sh` (downloaded images and the vendor map are not cleaned, this is mainly to be able to do it all again)
- if you really want to clear everything (also removes OTHER docker images): `docker rmi -f $(docker images | awk '{print $3}' | tail -n +2)` and `rm -rf vendor/*` (or just remove the clone dir)

### Use it as a demo
To give a demo one would:
- have docker installed
- have run `./init.sh` (this takes around 4 minutes first time)
- have opened [Kibana](http://localhost:5601/)
- have navigated to 'Dashboard' under 'Dashboard' [direct link](http://localhost:5601/app/kibana#/dashboard/dashboard?_g=(refreshInterval:('$$hashKey':'object:300',display:'5%20seconds',pause:!f,section:1,value:5000),time:(from:now-15m,mode:quick,to:now))&_a=(description:'',filters:!(),options:(darkTheme:!f),panels:!((col:1,id:baked_goods,panelIndex:1,row:1,size_x:6,size_y:3,type:visualization),(col:7,id:eaten_full,panelIndex:2,row:1,size_x:6,size_y:3,type:visualization),(col:1,id:baked_goods_time,panelIndex:3,row:4,size_x:6,size_y:3,type:visualization),(col:7,id:eaten_full_time,panelIndex:4,row:4,size_x:6,size_y:3,type:visualization)),query:(match_all:()),timeRestore:!f,title:Dashboard,uiState:(),viewMode:view))
- have set the interval to 'Last 15 minutes' and 'Auto refresh' to 5 seconds (both top right)
- have a terminal open to run `./feed.sh`
- give a talk, reveal the kibana dashboard which show four frowning smileys instead of graphs, 'cause there's no data yet
- run the `./feed.sh` command (either on or off screen)
- see the graphs update!
- do a `docker ps` to show all containers working for this demo
- show off [cAdvisor](http://localhost:8080) just because we can 

#### Extended demo
- run feed multiple times: `./feed.sh & ./feed.sh & ./feed.sh` gives a rate of event creation the 8 workers can't keep up with
- see [queue view](http://localhost:15672/#/queues/%2F/enqueue.app.default) to see the line going up, let it go to around 200
- do a `docker-compose up -d --scale app=32` 
- watch the queue go do down fast
- `docker-compose up -d --scale app=8` should scale back, but for me it often creates extra workers 
`docker-compose scale app=8` works but is deprecated

### URLs
- [Kibana](http://localhost:5601)
- [Rabbitmq](http://localhost:15672) (guest/guest)
- [Elasticsearch](http://localhost:9200)
- [cAdvisor](http://localhost:8080)

### Notes
Note: this is demo might not follow all best practices, nor is it complete in any way.
It is what it is: a demo :)

For example:
- would be handy to either generate useful output or use fluentd's parser to extract info from the log lines,
as we now need to use filters in Kibana
- the processors can be optimized in lots of ways
- I'm not feeling I'm using enqueue precisely in the best way

Also: only tested on MacOS.

### Author / License
Made by Ewald Börger as a demo at [Extendas](http://www.extendas.com/)

License: [CC-BY](https://creativecommons.org/licenses/by/3.0/)

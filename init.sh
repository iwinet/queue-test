echo "Setting up"

hosts_done=`grep -c symfony.dev /etc/hosts`
if [ "0" == "$hosts_done" ]
then
    echo "Need to add symfony.dev to your /etc/hosts file, for which we require your password"
    echo '127.0.0.1 symfony.dev' | sudo tee -a /etc/hosts >> /dev/null
fi

echo "Build machines, just in case you were editing something and been wondering for half an hour why nothing changes"
docker-compose build

echo "Running composer install (in a container so it is the correct PHP version)"
docker-compose run app composer install

echo "Starting the rest"
docker-compose up -d --scale app=0

echo "Importing kibana config"
cd docker/kibana
./load.sh
cd ../..

echo "Scaling up"
docker-compose up -d --scale app=8

echo "Ready"

echo "Open Kibana http://localhost:5601/app/kibana#/dashboard/dashboard?_g=(refreshInterval:('$$hashKey':'object:300',display:'5%20seconds',pause:!f,section:1,value:5000),time:(from:now-15m,mode:quick,to:now))&_a=(description:'',filters:!(),options:(darkTheme:!f),panels:!((col:1,id:baked_goods,panelIndex:1,row:1,size_x:6,size_y:3,type:visualization),(col:7,id:eaten_full,panelIndex:2,row:1,size_x:6,size_y:3,type:visualization),(col:1,id:baked_goods_time,panelIndex:3,row:4,size_x:6,size_y:3,type:visualization),(col:7,id:eaten_full_time,panelIndex:4,row:4,size_x:6,size_y:3,type:visualization)),query:(match_all:()),timeRestore:!f,title:Dashboard,uiState:(),viewMode:view))"
echo "Optionally open rabbitmq http://localhost:15672/#/queues/%2F/enqueue.app.default"
echo "Optionally open cAdvisor http://localhost:8080"
echo "run ./feed.sh"
